package business;

import javax.ejb.ActivationConfigProperty;
import javax.ejb.EJB;
import javax.ejb.MessageDriven;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.MessageListener;
import javax.jms.ObjectMessage;
import javax.jms.TextMessage;

import data.OrderDataService;
import models.Order;

/**
 * Message-Driven Bean implementation class for: OrderMessageService
 */
@MessageDriven(
		activationConfig = { @ActivationConfigProperty(
				propertyName = "destination", propertyValue = "Order"), @ActivationConfigProperty(
				propertyName = "destinationType", propertyValue = "javax.jms.Queue")
		},
		mappedName = "java:/jms/queue/Order")
public class OrderMessageService implements MessageListener {
	
	@EJB
	OrderDataService service;

    /**
     * Default constructor. 
     */
    public OrderMessageService() {
        // TODO Auto-generated constructor stub
    }
	
	/**
     * @see MessageListener#onMessage(Message)
     */
    public void onMessage(Message message) {
    	try {
    		if (message instanceof TextMessage) {
    			System.out.println("===========> OrderMessageService.onMessage() with a TextMessage: " + ((TextMessage)message).getText());
    			} else if (message instanceof ObjectMessage) {
    			System.out.println("===========> OrderMessageService.onMessage() with a Send Order Message: ");
    			service.create((Order)((ObjectMessage)message).getObject());
    			} else {
    			System.out.println("============> OrderMessageService.onMessage() with Uknown Message Type");
    			}
    	} catch (JMSException e) { 
        e.printStackTrace();
    	}
    }
}
