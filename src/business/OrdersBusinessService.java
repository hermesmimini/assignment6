package business;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.enterprise.inject.Alternative;
import javax.inject.Inject;

import data.DataAccessInterface;
import models.Order;

/**
 * Session Bean implementation class OrdersBusinessService
 */
@Stateless
@Local(OrdersBusinessInterface.class)
@Alternative
public class OrdersBusinessService implements OrdersBusinessInterface {
	
	@Inject
	DataAccessInterface<Order> orderDataService;
	
	
    /**
     * Default constructor. 
     */
    public OrdersBusinessService() {}
    
    

	/**
     * @see OrdersBusinessInterface#test()
     */
    public void test() {
        System.out.println("================> Hello from OrdersBusinessService.test()");
    }



	@Override
	public List<Order> getOrders() {
		
		return orderDataService.findAll();
	}



	@Override
	public void setOrders(List<Order> orders) {
				
	}

}
