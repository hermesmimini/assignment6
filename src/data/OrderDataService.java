package data;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import models.Order;

@Stateless
@Local(DataAccessInterface.class)
@LocalBean
public class OrderDataService implements DataAccessInterface<Order> {

	@Override
	public List<Order> findAll() {
		System.out.println("==================CALLING findAll()==================");
		Connection conn = null;
		String url = "jdbc:mysql://localhost:3306/testApp";
		String username = "root";
		String password = "root";
		String sql = "SELECT * FROM orders";
		List<Order> orders = new ArrayList<Order>();
		
		try 
		{
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(sql);
			while (rs.next()) {
				orders.add(new Order(
						rs.getInt("ID"),
						rs.getString("ORDER_NO"),
						rs.getString("PRODUCT_NAME"),
						rs.getFloat("PRICE"),
						rs.getInt("QUANTITY")));
			}
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally 
		{
			if (conn != null)
			{
				try
				{
					conn.close();
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}
			}
		}
		return orders;
	}

	@Override
	public Order findById(int id) {
		return null;
	}

	@Override
	public boolean create(Order order) {
		Connection conn = null;
		String url = "jdbc:mysql://localhost:3306/testApp";
		String username = "root";
		String password = "root";
		String sql = String.format("INSERT INTO orders(ORDER_NO, PRODUCT_NAME, PRICE, QUANTITY)"
				+ " VALUES('%s', '%s', %f, %d)", 
				order.getOrderNo(),
				order.getProductName(), 
				order.getPrice(), 
				order.getQuantity());
		
		try 
		{
			conn = DriverManager.getConnection(url, username, password);
			Statement stmt = conn.createStatement();
			stmt.executeUpdate(sql);
		}
		catch (SQLException e)
		{
			e.printStackTrace();
		}
		finally 
		{
			if (conn != null)
			{
				try
				{
					conn.close();
				}
				catch (SQLException e)
				{
					e.printStackTrace();
				}
			}
	}
		return true;
	}

	@Override
	public boolean update(Order order) {
		return true;
	}

	@Override
	public boolean delete(Order order) {
		return false;
	}

}
